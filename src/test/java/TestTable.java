/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.jirat.oxprogram.Player;
import com.jirat.oxprogram.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ACER
 */
public class TestTable {
    
    public TestTable() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    //@BeforeAll
    //public static void setUpClass() {
    //}
    
    //@AfterAll
    //public static void tearDownClass() {
    //}
    
    //@BeforeEach
    //public void setUp() {
    //}
    
    //@AfterEach
    //public void tearDown() {
    //}

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());

    }
    
    public void testRow2ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());

    }
    
    public void testRow3ByX() {
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());

    }
    
    public void testCol1ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
    
    public void testCol2ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
    
    public void testCol3ByO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
        
    }
    
    
    public void testSlashX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(x,table.getWinner());
    }
    
    public void testBackSlashO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(o, x);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(o,table.getWinner());
    }
    
    public void testDraw(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x,o);
        table.setRowCol(0, 0);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.switchPlayer();
        table.setRowCol(2, 0);
        table.switchPlayer();
        table.setRowCol(1, 0);
        table.switchPlayer();
        table.setRowCol(1, 1);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.switchPlayer();
        table.setRowCol(2, 1);
        table.switchPlayer();
        table.setRowCol(2, 2);
        table.switchPlayer();
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true,table.isFinish());
        assertEquals(null , table.getWinner());
        
        
    }
}
