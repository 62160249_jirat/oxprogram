/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirat.oxprogram;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class Game {

    Player playerX;
    Player playerO;
    Player currentPlayer;
    Player turn;
    Table table;
    int row, col;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }
    
    public void showTable() {
        table.showTable();
    }
    public void input() {
        while (true) {
            
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (row < 3 && col < 3 && row >= 0 && col >= 0) {
                if (table.setRowCol(row, col)) {
                    break;
                }
                System.out.println("Error: table at row and col is not emply!");
            } else {
                System.out.println("Row and Col must be 1-3.Please try again!");
            }
        }
    }
    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }
    
    public void newGame() {
        table = new Table(playerX, playerO);
        this.showWelcome();
        table.switchPlayer();
    }
    
     public void endGame() {
        System.out.println("Bye bye....");
    }
    
    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if(table.isFinish()) {
                if(table.getWinner()==null) {
                    this.showTable();
                    System.out.println("Draw!!");
                }else {
                    this.showTable();
                    System.out.println(table.getWinner().getName() + " Win!!");
                }

                System.out.println("-----------------------------------------");
                System.out.println("Do you want to NewGame??");
                System.out.println("Yes/No: ");
                String ans = kb.next();
                if (ans.equals("Yes")) {
                    this.newGame();
                } else if (ans.equals("No")) {
                    this.endGame();
                    break;
                }

            }
            table.switchPlayer();
        }

        
    }
}
